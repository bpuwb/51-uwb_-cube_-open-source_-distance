#include "bphero_uwb.h"
#include "port.h"
#include <math.h>
/* Includes ------------------------------------------------------------------*/
#include "spi.h"
int psduLength = 0;//发送数据长度，发送的时候需要动态调整
srd_msg_dsss msg_f_send ; // ranging message frame with 16-bit addresses
uint8 rx_buffer[FRAME_LEN_MAX];//保存接收到的字符
uint32 status_reg = 0;//DW1000 状态寄存器
uint16 frame_len = 0;//接收到数据长度

//这个函数把UWB后期发送的数据结构进行打包处理
//对于用户发送的数据只需要放置到msg_f_send.messageData 中
//数据格式定义来源于IEEE标准
void BPhero_UWB_Message_Init(void)
{
    //set frame type (0-2), SEC (3), Pending (4), ACK (5), PanIDcomp(6)
    msg_f_send.frameCtrl[0] = 0x1 /*frame type 0x1 == data*/ | 0x40 /*PID comp*/|0x20/* ACK request*/;
    //source/dest addressing modes and frame version
    msg_f_send.frameCtrl[1] = 0x8 /*dest extended address (16bits)*/ | 0x80 /*src extended address (16bits)*/;
    msg_f_send.panID[0] = NET_PANID & 0xFF;
    msg_f_send.panID[1] = (NET_PANID>>8) & 0xFF;

    msg_f_send.seqNum = 0;
    psduLength = (TAG_POLL_MSG_LEN + FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC);

    msg_f_send.seqNum = 0; //copy sequence number and then increment
    msg_f_send.sourceAddr[0] = SHORT_ADDR & 0xFF; //copy the address
    msg_f_send.sourceAddr[1] =(SHORT_ADDR>>8)& 0xFF; //copy the address

    msg_f_send.destAddr[0] = 0x01;  //set the destination address (broadcast == 0xffff)
    msg_f_send.destAddr[1] = 0x01;  //set the destination address (broadcast == 0xffff)
	psduLength = 11;
}
/* Default communication configuration */
//UWB工作信息配置，
//工作频段：channel2
//传输码率: 110Kbps
//其它通常不需要修改
dwt_config_t config =
{
    2,               /* Channel number. */
    DWT_PRF_64M,     /* Pulse repetition frequency. */
    DWT_PLEN_1024,   /* Preamble length. */
    DWT_PAC32,       /* Preamble acquisition chunk size. Used in RX only. */
    9,               /* TX preamble code. Used in TX only. */
    9,               /* RX preamble code. Used in RX only. */
    1,               /* Use non-standard SFD (Boolean) */
    DWT_BR_110K,     /* Data rate. */
    //DWT_BR_6M8,      /* Data rate. */
    DWT_PHRMODE_STD, /* PHY header mode. */
    (1025 + 64 - 32) /* SFD timeout (preamble length + 1 + SFD length - PAC size). Used in RX only. */
};


void reset_DW1000(void)
{
    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(UWB_RESET_GPIO_Port, UWB_RESET_Pin, GPIO_PIN_SET);
    HAL_Delay(20);
    HAL_GPIO_WritePin(UWB_RESET_GPIO_Port, UWB_RESET_Pin, GPIO_PIN_RESET);
    HAL_Delay(20);
    HAL_GPIO_WritePin(UWB_RESET_GPIO_Port, UWB_RESET_Pin, GPIO_PIN_SET);
}

//extern void dwt_enable_pa(void);
extern void dwt_SetTxPower(dwt_config_t *config);
//DW1000 芯片初始化函数
void BPhero_UWB_Init(void)//dwm1000 init related
{
    //通过STM32 GPIO对DW1000 进行初始化
    reset_DW1000();
    //配置STM32 SPI接口，对DW1000 初始化时SPI接口速率不能超过3M

    MX_SPI1_Init();
    printf("hello dwm1000!\r\n");
    //关键参数初始化，直接调用decawave API完成
    if(dwt_initialise(DWT_LOADUCODE) == DWT_ERROR)
    {
        printf("dwm1000 init fail!\r\n");
//        OLED_ShowString(0,0,"INIT FAIL");
//        while (1)
//        {
//             HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2);
//             deca_sleep(1000);
//        }
    }
    printf("init pass!\r\n");
    //配置STM32 SPI接口速率，当对DW1000 基本初始化完成后，可以将速率提高到18M
    MX_SPI1_Init_Fast();
    //配置DW1000 工作的频点 等相关信息
    dwt_configure(&config);
    //启动DW1000 RX TX状态指示灯
    //DW1000 一共有4个LED灯，可以通过这个函数开启，默认开启两个
    dwt_setleds(1);
    //配置发送功率，使用最大发射功率，具体可以参考DW1000 数据手册
    dwt_SetTxPower(&config);
    //配置DW1000 工作的网络ID:PANID
    dwt_setpanid(NET_PANID);
    //配置DW1000 自身的短地址
    dwt_setaddress16(SHORT_ADDR);
    //配置天线延时，可能需要根据实际距离差异进行偏移调整
    dwt_setrxantennadelay(RX_ANT_DLY);
    dwt_settxantennadelay(TX_ANT_DLY);
    //配置DW1000 中断源 ---> 只有DW1000 内部配置好，才会拉中断引脚，具体可以参考DW1000 数据手册
    dwt_setinterrupt(DWT_INT_RFCG | (DWT_INT_ARFE | DWT_INT_RFSL | DWT_INT_SFDT | DWT_INT_RPHE | DWT_INT_RFCE | DWT_INT_RFTO /*| DWT_INT_RXPTO*/), 1);
    dwt_enable_pa();
}


/************************************************************************/
/************************************************************************/
/**********************计算接收到信息的信号强度:RSSI**********************/
/********************************BEGIN***********************************/
/************************************************************************/

static float calculatePower(float base, float N, uint8_t pulseFrequency) {
    float A, corrFac;

    if(DWT_PRF_16M == pulseFrequency) {
        A = 115.72;
        corrFac = 2.3334;
    } else {
        A = 121.74;
        corrFac = 1.1667;
    }

    float estFpPwr = 10.0 * log10(base / (N * N)) - A;

    if(estFpPwr <= -88) {
        return estFpPwr;
    } else {
        // approximation of Fig. 22 in user manual for dbm correction
        estFpPwr += (estFpPwr + 88) * corrFac;
    }

    return estFpPwr;
}

float dwGetReceivePower(void) {
    dwt_rxdiag_t diagnostics;
    dwt_readdiagnostics(&diagnostics);
    float C = (&diagnostics.stdNoise)[3];
    float N = diagnostics.rxPreamCount;

    float twoPower17 = 131072.0;
    return calculatePower(C * twoPower17, N, config.prf);
}
/**********************计算接收到信息的信号强度:RSSI**********************/
/*********************************END***********************************/
