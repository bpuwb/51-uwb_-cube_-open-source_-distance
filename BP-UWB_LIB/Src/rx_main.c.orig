/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "deca_device_api.h"
#include "deca_regs.h"
#include "deca_sleep.h"
#include "port.h"
#ifdef LCD_ENABLE
#include "lcd_oled.h"
#endif
#include "frame_header.h"
#include "bp_filter.h"
#include "common_header.h"
#include "bphero_uwb.h"
#include "dwm1000_timestamp.h"
#include "kalman.h"
/******************************************************************************************************************
********************* NOTES on DW (MP) features/options ***********************************************************
*******************************************************************************************************************/
//TWR过程中用到的6个时间戳，RX TX NODE一共用到6个，这里用static定义，放置和tx_main.c冲突
//！！！TWR 定位原理说明，请参见51uwb.cn视频讲解！！！
//以下6个变量用来记录时间戳信息
static uint64 poll_rx_ts;
static uint64 resp_tx_ts;
static uint64 final_rx_ts;
//static uint64 poll_tx_ts;
//static uint64 resp_rx_ts;
//static uint64 final_tx_ts;

static srd_msg_dsss *msg_f ;
static double tof;
static double distance_temp = 0;
static double distance[256] = {0};
extern dwt_config_t config;
/*******************************************************************************
* 函数名  : Simple_Rx_Callback
* 描述    : RX 节点DW1000 中断回调函数
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 当节点DW1000收到信息后会触发中断调用到这个函数
*******************************************************************************/
void Simple_Rx_Callback()
{
    uint32 status_reg = 0,i=0;
    //计算距离的中间变量
    uint32 poll_tx_ts, resp_rx_ts, final_tx_ts;
    uint32 poll_rx_ts_32, resp_tx_ts_32, final_rx_ts_32;
    double Ra, Rb, Da, Db;
    int64 tof_dtu;

    //定义变量用于处理broadcast 信息
	  uint16 Sourceaddress = 0x0000;
    uint8 Num_Anthor = 0;
    uint8 Index= 0;
    uint8 *pAnthor_Str ;
    uint8 match_flag = 0;

    //清空接收buffer
    for (i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    dwt_enableframefilter(DWT_FF_NOTYPE_EN);//关闭帧过滤功能
    status_reg = dwt_read32bitreg(SYS_STATUS_ID);//读取状态表示

    if (status_reg & SYS_STATUS_RXFCG)//正确收到数据，且通过帧过滤
    {
        /* A frame has been received, copy it to our local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;//获取接收到数据长度
        if (frame_len <= FRAME_LEN_MAX)//数据长度判定
        {
            dwt_readrxdata(rx_buffer, frame_len, 0);//将接收到的数据读取到rx_buffer中
            msg_f = (srd_msg_dsss*)rx_buffer;
            msg_f_send.destAddr[0] = msg_f->sourceAddr[0];//提取该数据的发送节点短地址
            msg_f_send.destAddr[1] = msg_f->sourceAddr[1];
            msg_f_send.seqNum = msg_f->seqNum;//提取该数据的发送seq num
            switch(msg_f->messageData[0])//通过用户自定义的第一个数据判定为何种信息
            {

            case 'B':
                // printf("receive B anthor = %d\n",msg_f->messageData[1]);
                Num_Anthor = msg_f->messageData[1];
                Sourceaddress =  msg_f->sourceAddr[1]<<8| msg_f->sourceAddr[0];
                pAnthor_Str = &msg_f->messageData[2];
                match_flag = 0;
                for (Index = 0; Index < Num_Anthor; Index++)
                {
                    //  printf("receive address = %04X\n",(pAnthor_Str[1]<<8|pAnthor_Str[0]));

                    if(SHORT_ADDR == (pAnthor_Str[1]<<8|pAnthor_Str[0])) //匹配成功
                    {
                        //    printf("match\n");
                        match_flag = 1;

                    }
                    pAnthor_Str = pAnthor_Str +3 ;
                }

                if(match_flag == 0)//没有匹配到，发送一个反馈信息
                {
                    msg_f_send.messageData[0]='R';//Poll message
                    //后面修改这个数据长度
                    dwt_writetxdata(11 + 1, (uint8 *)&msg_f_send, 0) ; // write the frame data
                    dwt_writetxfctrl(11 + 1, 0);
                    dwt_starttx(DWT_START_TX_IMMEDIATE);
                    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                    { };
                }
                break;


            //标签发送来的poll 信息，基站收到标签发送的Poll信息后，会给标签回复Ack应答信息)
            case 'P':
                msg_f_send.messageData[0]='A';//Poll ack message
                //除了应答以外，还将上次测得的距离信息一起打包发送给标签
                int temp = (int)(distance[msg_f_send.destAddr[0]]*100);//convert m to cm
                msg_f_send.messageData[1]=temp/100;
                msg_f_send.messageData[2]=temp%100;
                //写入数据
                dwt_writetxdata(14, (uint8 *)&msg_f_send, 0) ; // write the frame data
                dwt_writetxfctrl(14, 0);
                dwt_starttx(DWT_START_TX_IMMEDIATE);//启动发送
                while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS)) //等待定时信息发送完成
                { };
                dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);//清除发送完成标志
                poll_rx_ts = get_rx_timestamp_u64();//读取接收到该信息的时间戳
                break;
            //标签发送来的final信息，基站收到标签发送的final信息后，解包内部数据结合自身时间戳计算与标签的距离
            case 'F':
                resp_tx_ts = get_tx_timestamp_u64(); //读取上次发送信息的时间戳
                final_rx_ts = get_rx_timestamp_u64();//读取接收到该信息的时间戳
                //将Final message中的包含的数据解包
                final_msg_get_ts(&msg_f->messageData[FINAL_MSG_POLL_TX_TS_IDX], &poll_tx_ts);
                final_msg_get_ts(&msg_f->messageData[FINAL_MSG_RESP_RX_TS_IDX], &resp_rx_ts);
                final_msg_get_ts(&msg_f->messageData[FINAL_MSG_FINAL_TX_TS_IDX], &final_tx_ts);
                //计算距离
                poll_rx_ts_32 = (uint32)poll_rx_ts;
                resp_tx_ts_32 = (uint32)resp_tx_ts;
                final_rx_ts_32 = (uint32)final_rx_ts;
                Ra = (double)(resp_rx_ts - poll_tx_ts);
                Rb = (double)(final_rx_ts_32 - resp_tx_ts_32);
                Da = (double)(final_tx_ts - resp_rx_ts);
                Db = (double)(resp_tx_ts_32 - poll_rx_ts_32);
                tof_dtu = (int64)((Ra * Rb - Da * Db) / (Ra + Rb + Da + Db));
                tof = tof_dtu * DWT_TIME_UNITS;
                distance_temp = tof * SPEED_OF_LIGHT;
                //距离减去矫正系数
                //官方提供的校正参考，可以根据实际情况略微修改
                //如果测得距离和实际距离有较大差异，但是测得距离较稳定，首先考虑天线延时校正
                //天线延时校正参考视频https://www.bilibili.com/video/bv1154y1q7Qp
                distance[msg_f_send.destAddr[0]] = distance_temp - dwt_getrangebias(config.chan,(float)distance_temp, config.prf);
                //对距离信息进行kalman滤波处理
                distance[msg_f_send.destAddr[0]] = KalMan(distance[msg_f_send.destAddr[0]]);
                //distance[msg_f_send.destAddr[0]] = CKF(distance[msg_f_send.destAddr[0]]);
                //distance[msg_f_send.destAddr[0]] = ACKF(distance[msg_f_send.destAddr[0]]);
                break;
            //标签发送的三个距离信息-->只有地址为0x0001的基站才会收到这个信息
            case 'M':
                //将收到的距离信息通过串口发送给电脑上位机
                //这里发送长度为16字节，如果自己更改通信协议，记得修改这个长度

//										USART_puts(&msg_f->messageData[1],16);
                HAL_UART_Transmit(&UWB_USART, (uint8_t *)&msg_f->messageData[2], msg_f->messageData[1], 0xFFFF);
                break;

            default:
                break;
            }
        }
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));//清理接收标识
        dwt_enableframefilter(DWT_FF_DATA_EN);//使能帧过滤功能
        dwt_setrxtimeout(0);//设定timeout，值为0表示一直等待接收不会timeout
        dwt_rxenable(0);//开启DW1000接收器
    }
    else
    {
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));//清理接收标识
        dwt_enableframefilter(DWT_FF_DATA_EN);//使能帧过滤功能
        dwt_setrxtimeout(0);//设定timeout，值为0表示一直等待接收不会timeout
        dwt_rxenable(0);//开启DW1000接收器
    }
}
#ifdef RX_NODE
#include "tim.h"
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if(htim==(&UWB_htim))
    {
        HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);
    }
}
#endif
#include "main.h"
/*******************************************************************************
* 函数名  : rx_main
* 描述    : 基站主函数
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 1主要通过液晶显示信息
						2注册回调函数
						3使用定时器周期性发送信息
*******************************************************************************/
int rx_main(void)
{
#ifdef LCD_ENABLE
    OLED_ShowString(0,0,"   51UWB Node");
    OLED_ShowString(0,2,"  www.51uwb.cn");
    OLED_ShowString(0,4,"    Rx Node ");
#endif
    dwt_setrxtimeout(0); //设定timeout，值为0表示一直等待接收不会timeout
    //启动接收
    //Step1:启动帧过滤功能 --> 只接收数据包，更多帧过滤功能相关内容可以参考51uwb.cn
    dwt_enableframefilter(DWT_FF_DATA_EN);
    //Step2:设定接收延时，timeout参数为0表示一直处于接收状态
    dwt_setrxtimeout(0);
    //Step3:立马启动接收，这个函数里的参数可以设置延时接收，可以优化，让接收机过段时间启动，减少能量损耗
    dwt_rxenable(0);
    //如果使用kalman滤波需要先初始化
    //！！！！注意，这里的kalman仅能支持一对一，如果修改项目，有多个tx，这个kalman不能用！！！！
    //kalman 滤波器会保存之前的“历史" ,如果多个标签，历史记录会混乱
    //多标签或者多对多，出现数据混乱，第一时间把kalman去掉测试
    KalMan_Init();
    while (1)
    {
    }
    return true;
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */
/*****************************************************************************************************************************************************
 * NOTES:
 *
 * 1. In this example, maximum frame length is set to 127 bytes which is 802.15.4 UWB standard maximum frame length. DW1000 supports an extended
 *    frame length (up to 1023 bytes long) mode which is not used in this example.
 * 2. Manual reception activation is performed here but DW1000 offers several features that can be used to handle more complex scenarios or to
 *    optimise system's overall performance (e.g. timeout after a given time, automatic re-enabling of reception in case of errors, etc.).
 * 3. We use polled mode of operation here to keep the example as simple as possible but RXFCG and error/timeout status events can be used to generate
 *    interrupts. Please refer to DW1000 User Manual for more details on "interrupts".
 * 4. The user is referred to DecaRanging ARM application (distributed with EVK1000 product) for additional practical example of usage, and to the
 *    DW1000 API Guide for more details on the DW1000 driver functions.
 ****************************************************************************************************************************************************/
/**
  * @}
  */
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
