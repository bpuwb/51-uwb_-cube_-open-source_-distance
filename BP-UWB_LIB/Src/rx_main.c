/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "deca_device_api.h"
#include "deca_regs.h"
#include "deca_sleep.h"
#include "port.h"
#ifdef LCD_ENABLE
#include "lcd_oled.h"
#endif
#include "frame_header.h"
#include "bp_filter.h"
#include "common_header.h"
#include "bphero_uwb.h"
#include "dwm1000_timestamp.h"
#include "kalman.h"

//static void Handle_TimeStamp(void);
/*******************************************************************************************************************/
//TWR过程中用到的6个时间戳，RX TX NODE一共用到6个，这里用static定义，放置和tx_main.c冲突
//！！！TWR 定位原理说明，请参见51uwb.cn视频讲解！！！
//视频1 ： https://www.bilibili.com/video/BV1jt4y1y7v8/
//视频2 ： https://www.bilibili.com/video/BV1GT4y1J7Ky/
static uint64 poll_rx_ts;
static uint64 resp_tx_ts;
static uint64 final_rx_ts;

//static uint64 poll_tx_ts;
//static uint64 resp_rx_ts;
//static uint64 final_tx_ts;
/*******************************************************************************************************************/

static srd_msg_dsss *msg_f ;
static double tof;
static double distance;
extern dwt_config_t config;

/***************RX 节点 UWB中断处理函数**************/
void Simple_Rx_Callback()
{
    //定义存放UWB状态的变量
    uint32 status_reg = 0;
    //定义保存时间戳
    uint32 poll_tx_ts, resp_rx_ts, final_tx_ts;
    uint32 poll_rx_ts_32, resp_tx_ts_32, final_rx_ts_32;
    double Ra, Rb, Da, Db;
    int64 tof_dtu;
    int temp  = 0;//保存临时变量
    float uwb_rssi = 0;//定义保存RSSI信号强度的变量

    char dist_str[16] = {0};//字符串数组，用来存放LCD显示的数据
    //每次进入接收中断函数，先清理缓存区域，准备接收数据
    for (uint32 i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    //通过帧过滤功能，配置UWB不接收任何数据，相当于关闭接收
    dwt_enableframefilter(DWT_FF_NOTYPE_EN);//disable recevie
    //读取UWB状态寄存器
    status_reg = dwt_read32bitreg(SYS_STATUS_ID);
    //判断是否有完整数据接收完毕
    if (status_reg & SYS_STATUS_RXFCG)
    {
        //读取接收到的数据长度
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;
        //如果数据长度小于最大值，认为是合理的
        if (frame_len <= FRAME_LEN_MAX)
        {
            //读取接收到的数据
            dwt_readrxdata(rx_buffer, frame_len, 0);
            //将数据强制转换成约定格式
            msg_f = (srd_msg_dsss*)rx_buffer;
            //提取发送该数据的短地址，并赋值到将要发送信息的目标地址
            //哪里来的信息，后面回复给谁
            msg_f_send.destAddr[0] = msg_f->sourceAddr[0];
            msg_f_send.destAddr[1] = msg_f->sourceAddr[1];
            //提取sequence Num
            msg_f_send.seqNum = msg_f->seqNum;
            //因为我们TWR交互只用到第一个数据，所以这里只判断第一个数据就可以
            switch(msg_f->messageData[0])
            {
            case 'P'://字符P是rx节点tx节点POLL信息
                //保存接收到这个信息的时间戳
                poll_rx_ts = get_rx_timestamp_u64();
                msg_f_send.messageData[0]='A';//Poll ack message
                //将上次测距信息打包发送
                temp = (int)(distance*100);//convert m to cm
                msg_f_send.messageData[1]=temp/100;
                msg_f_send.messageData[2]=temp%100;
                //将要发送的数据写入到UWB寄存器内
                dwt_writetxdata(psduLength +3 , (uint8 *)&msg_f_send, 0) ; // write the frame data
                //告知UWB发送数据偏移为0
                dwt_writetxfctrl(psduLength +3, 0);
                //启动立即发送
                dwt_starttx(DWT_START_TX_IMMEDIATE);
                //等待发送完成
                //MUST WAIT!!!!!
                while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                { };
                break;

            case 'F'://字符F是rx节点tx节点Final信息
                //保存发送A信息的时间戳
                resp_tx_ts = get_tx_timestamp_u64();
                //保存接收‘F'信息的时间戳
                final_rx_ts = get_rx_timestamp_u64();
                //提取数据包载荷中时间戳信息
                final_msg_get_ts(&msg_f->messageData[FINAL_MSG_POLL_TX_TS_IDX], &poll_tx_ts);
                final_msg_get_ts(&msg_f->messageData[FINAL_MSG_RESP_RX_TS_IDX], &resp_rx_ts);
                final_msg_get_ts(&msg_f->messageData[FINAL_MSG_FINAL_TX_TS_IDX], &final_tx_ts);
                //根据TWR算法计算距离，这个部分可以参考51uwb.cn的视频讲解说明
                poll_rx_ts_32 = (uint32)poll_rx_ts;
                resp_tx_ts_32 = (uint32)resp_tx_ts;
                final_rx_ts_32 = (uint32)final_rx_ts;
                Ra = (double)(resp_rx_ts - poll_tx_ts);
                Rb = (double)(final_rx_ts_32 - resp_tx_ts_32);
                Da = (double)(final_tx_ts - resp_rx_ts);
                Db = (double)(resp_tx_ts_32 - poll_rx_ts_32);
                tof_dtu = (int64)((Ra * Rb - Da * Db) / (Ra + Rb + Da + Db));

                tof = tof_dtu * DWT_TIME_UNITS;
                distance = tof * SPEED_OF_LIGHT;
                //官方给出偏移校正，用户可以适当根据环境调整偏移表格数据
                distance = distance - dwt_getrangebias(config.chan,(float)distance, config.prf);//距离减去矫正系数
                //对计算的距离进行卡尔曼滤波
                //kalman filter
                distance = KalMan(distance);
                //将距离信息发送到串口
//                printf("0x%04X <--> 0x%02X%02X :%d cm\n",SHORT_ADDR,msg_f_send.destAddr[1],msg_f_send.destAddr[0],(int)(100*KalMan(distance)));
                temp = (int)(distance*100);
#ifdef LCD_ENABLE
                sprintf(dist_str, "  Dis: %.2f m", (float)temp/100);
                OLED_ShowString(0, 6,dist_str);
#endif
//                float uwb_rssi = 0;
                //读取RSSI信号强度并显示
                uwb_rssi = dwGetReceivePower();
#ifdef LCD_ENABLE
                sprintf(dist_str,"RSSI:%2.2fdB ",uwb_rssi);
                OLED_ShowString(0,4,dist_str);
#endif
//                printf("0x%04X <--> 0x%02X%02X :%d  cm\n",SHORT_ADDR,msg_f_send.destAddr[1],msg_f_send.destAddr[0],temp);
                break;
            default:
                break;
            }
        }
    }
    //如果是异常数据，清理各种接收标识
    dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));
    //重新使能接收，timeout
    //Step1:启动帧过滤功能 --> 只接收数据包，更多帧过滤功能相关内容可以参考51uwb.cn
    dwt_enableframefilter(DWT_FF_DATA_EN);
    //Step2:设定接收延时，timeout参数为0表示一直处于接收状态
    dwt_setrxtimeout(0);
    //Step3:立马启动接收，这个函数里的参数可以设置延时接收，可以优化，让接收机过段时间启动，减少能量损耗
    dwt_rxenable(0);
}

#ifdef RX_NODE
#include "tim.h"
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if(htim==(&UWB_htim))
    {
        HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
    }
}
#endif

#include "main.h"
int rx_main(void)
{
#ifdef LCD_ENABLE
    OLED_ShowString(0,0,"   51UWB Node");
    OLED_ShowString(0,2,"  www.51uwb.cn");
    OLED_ShowString(0,4,"    Rx Node ");
#endif
    dwt_setrxtimeout(0);
    //启动接收
    //Step1:启动帧过滤功能 --> 只接收数据包，更多帧过滤功能相关内容可以参考51uwb.cn
    dwt_enableframefilter(DWT_FF_DATA_EN);
    //Step2:设定接收延时，timeout参数为0表示一直处于接收状态
    dwt_setrxtimeout(0);
    //Step3:立马启动接收，这个函数里的参数可以设置延时接收，可以优化，让接收机过段时间启动，减少能量损耗
    dwt_rxenable(0);
    //如果使用kalman滤波需要先初始化
    //！！！！注意，这里的kalman仅能支持一对一，如果修改项目，有多个tx，这个kalman不能用！！！！
    //kalman 滤波器会保存之前的“历史" ,如果多个标签，历史记录会混乱
    //多标签或者多对多，出现数据混乱，第一时间把kalman去掉测试
	  UWB_TIM_Init();
    HAL_TIM_Base_Start_IT(&UWB_htim);
    KalMan_Init();
    while (1)
    {
    }
    return true;
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
/******************* (C) COPYRIGHT 2011 51uwb.cn *****END OF FILE****/
